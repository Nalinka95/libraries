package com.company.dao;

import com.company.bo.Admin;
import com.company.bo.LibraryBookRecord;

import java.util.ArrayList;
import java.util.List;

public class LibraryBookRecordDao {
    private static List<LibraryBookRecord> libraryBookRecords;

    private LibraryBookRecordDao() {
        libraryBookRecords = new ArrayList<LibraryBookRecord>();
    }

    public static List<LibraryBookRecord> getLibraryBookRecords() {
        return libraryBookRecords;
    }

    public void addLibraryBook(LibraryBookRecord newLibraryBook){
        libraryBookRecords.add(newLibraryBook);
    }

    public void updateLibraryBook(LibraryBookRecord updateLibraryBook){
        for(int index=0;index<libraryBookRecords.size();index++){
            if(libraryBookRecords.get(index).getLibraryBook().getBookName().equalsIgnoreCase(updateLibraryBook.getLibraryBook().getBookName())){
                libraryBookRecords.add(index,updateLibraryBook);
                return;
            }
        }
    }
    public void removeLibraryBook(LibraryBookRecord removeLibraryBook){
        libraryBookRecords.remove(removeLibraryBook);
    }

    public LibraryBookRecord findLibraryBook(String bookName){
        for(LibraryBookRecord currentLibraryBook:libraryBookRecords){
            if(currentLibraryBook.getLibraryBook().getBookName().equalsIgnoreCase(bookName))
                return currentLibraryBook;
        }
        throw new NullPointerException("Book name is invalid");

    }
}

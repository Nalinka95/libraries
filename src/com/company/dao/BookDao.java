package com.company.dao;

import com.company.bo.Admin;
import com.company.bo.Book;

import java.util.ArrayList;
import java.util.List;

public class BookDao {
    private List<Book> books;
    private static BookDao bookDao;

    private BookDao() {
        books = new ArrayList<Book>();
    }

    public static BookDao getBookDao() {
        bookDao = bookDao == null ? new BookDao() : bookDao;
        return bookDao;
    }

    public void addBook(Book newBook) {
        books.add(newBook);
    }

    public void updateBook(Book updateBook) {
        for (int index = 0; index < books.size(); index++) {
            if (books.get(index).getBookName().equalsIgnoreCase(updateBook.getBookName())) {
                books.set(index, updateBook);
                return;
            }
        }
    }

    public void removeBook(Book removeBook) {
        books.remove(removeBook);
    }

    public Book findBook(Book book) {
        for (Book currentBook : books) {
            if (currentBook.getBookName().equalsIgnoreCase(book.getBookName()))
                return currentBook;
        }
        throw new NullPointerException("Id is invalid");

    }

    public Book findBookByName(String bookName) {
        for (Book currentBook : books) {
            if (currentBook.getBookName().equalsIgnoreCase(bookName))
                return currentBook;
        }
        throw new NullPointerException("Id is invalid");

    }

    public boolean isValidBook(Book book) {
        return findBook(book) != null;
    }
}

package com.company.dao;

import com.company.bo.Admin;
import com.company.bo.IssuedBookRecord;

import java.util.ArrayList;
import java.util.List;

public class IssueBookRecordDao {
    private List<IssuedBookRecord> issuedBookRecords;
    private static IssueBookRecordDao issueBookRecordDao;
    private int issuedBookId;
    private IssueBookRecordDao() {
        issuedBookRecords=new ArrayList<IssuedBookRecord>();
    }

    public static IssueBookRecordDao getIssueBookRecordDao() {
        issueBookRecordDao= issueBookRecordDao==null ? new IssueBookRecordDao():issueBookRecordDao;
        return issueBookRecordDao;
    }

    public void addIssuedBook(IssuedBookRecord newIssuedBook){
        issuedBookRecords.add(newIssuedBook);
    }

    public void updateIssuedBook(IssuedBookRecord updateIssuedBook){
        for(int index=0;index<issuedBookRecords.size();index++){
            IssuedBookRecord currentBook= issuedBookRecords.get(index);
            if(currentBook.getBorrowedStudent().getRollNumber()==updateIssuedBook.getBorrowedStudent().getRollNumber() && currentBook.getIssuedBook().getBookName().equalsIgnoreCase(updateIssuedBook.getIssuedBook().getBookName())){
                issuedBookRecords.add(index,updateIssuedBook);
                return;
            }
        }
    }
    public void removeIssuedBook(IssuedBookRecord removeIssuedBook){
        issuedBookRecords.remove(removeIssuedBook);
    }

    public IssuedBookRecord findIssuedBook(IssuedBookRecord issuedBookRecord){
        for(IssuedBookRecord currentBook:issuedBookRecords){
            if(currentBook.getIssuedBook().getBookName().equalsIgnoreCase(issuedBookRecord.getIssuedBook().getBookName())&& currentBook.getBorrowedStudent().getRollNumber()==issuedBookRecord.getBorrowedStudent().getRollNumber())
                return currentBook;
        }
        throw new NullPointerException("Id is invalid");

    }
}

package com.company.dao;

import com.company.bo.Admin;
import com.company.bo.Library;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class AdminDao {
    private static AdminDao adminDao;
    private List<Admin> admins;

    private AdminDao() {

        admins = new ArrayList<Admin>();
    }

    public static AdminDao getInstance() {
        adminDao = adminDao == null ? new AdminDao() : adminDao;
        return adminDao;
    }

    public void addAdmin(Admin newAdmin) {
        admins.add(newAdmin);
    }

    public void updateAdmin(Admin updateAdmin) {
        for (int index = 0; index < admins.size(); index++) {
            if (admins.get(index).getIdentityNumber() == updateAdmin.getIdentityNumber()) {
                admins.set(index, updateAdmin);
                return;
            }
        }
    }

    public void removeAdmin(Admin removeAdmin) {
        admins.remove(removeAdmin);
    }

    public Admin findAdmin(Admin admin) {
        for (Admin currentAdmin : admins) {
            if (currentAdmin.getIdentityNumber() == admin.getIdentityNumber())
                return currentAdmin;
        }
        return null;

    }

    public Admin findAdminByName(String adminName) {
        for (Admin currentAdmin : admins) {
            if (currentAdmin.getAdminName().equalsIgnoreCase(adminName))
                return currentAdmin;
        }
        return null;

    }

    public Admin findAdminById(int adminId) {
        for (Admin currentAdmin : admins) {
            if (currentAdmin.getIdentityNumber() == adminId)
                return currentAdmin;
        }
        return null;

    }

    public boolean isValidAdmin(Admin admin) {
        return findAdmin(admin) != null;
    }


}



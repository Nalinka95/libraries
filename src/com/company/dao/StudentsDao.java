package com.company.dao;

import com.company.bo.Admin;
import com.company.bo.Student;

import java.util.ArrayList;
import java.util.List;

public class StudentsDao {
    private static List<Student> students;

    private StudentsDao() {
        students= new ArrayList<Student>();
    }

    public static List<Student> getStudents() {
        return students;
    }

    public void addStudent(Student newStudent){
        students.add(newStudent);
    }

    public void updateStudent(Student updateAStudent){
        for(int index=0;index<students.size();index++){
            if(students.get(index).getRollNumber()==updateAStudent.getRollNumber()){
                students.add(index,updateAStudent);
                return;
            }
        }
    }
    public void removeStudent(Student removeStudent){
        students.remove(removeStudent);
    }

    public Student findStudent(Student student){
        for(Student currentStudent:students){
            if(currentStudent.getRollNumber()==student.getRollNumber())
                return currentStudent;
        }
        throw new NullPointerException("Roll number is invalid");

    }
    public boolean isValidStudent(Student student){
        return findStudent(student) != null;
    }
}

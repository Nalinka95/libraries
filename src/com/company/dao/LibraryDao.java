package com.company.dao;

import com.company.bo.Admin;
import com.company.bo.Library;

import java.util.ArrayList;
import java.util.List;

public class LibraryDao {
    private static List<Library> libraries;

    private LibraryDao() {
        libraries= new ArrayList<Library>();
    }

    public static List<Library> getLibraries() {
        return libraries;
    }

    public void addLibrary(Library newLibrary){
        libraries.add(newLibrary);
    }

    public void updateLibrary(Library updateLibrary){
        for(int index=0;index<libraries.size();index++){
            if(libraries.get(index).getLibraryId()==updateLibrary.getLibraryId()){
                libraries.add(index,updateLibrary);
                return;
            }
        }
    }
    public void removeLibrary(Library removeLibrary){
        libraries.remove(removeLibrary);
    }

    public Library findLibrary(Library library){
        for(Library currentLibrary:libraries){
            if(currentLibrary.getLibraryId()==library.getLibraryId())
                return currentLibrary;
        }
        return null;

    }
    public boolean isValidLibrary(Library library){
        return findLibrary(library) != null;
    }
}

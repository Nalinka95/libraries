package com.company.bo;

import com.company.bo.enums.Subject;

import java.util.Objects;

public class Book {
    private String bookName;
    private Subject subject;

    public Book(String bookName, Subject subject) {
        setBookName(bookName);
        setSubject(subject);

    }

    public String getBookName() {
        return bookName;
    }

    private void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public Subject getSubject() {
        return subject;
    }

    private void setSubject(Subject subject) {
        this.subject = subject;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Book)) return false;
        Book book = (Book) o;
        return Objects.equals(getBookName(), book.getBookName()) &&
                getSubject() == book.getSubject();
    }
}

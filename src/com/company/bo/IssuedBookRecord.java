package com.company.bo;

import com.company.bo.enums.Subject;

import java.util.List;
import java.util.Objects;

public class IssuedBookRecord {
    private Admin bookIssuedAdmin;
    private Book issuedBook;
    private Student borrowedStudent;
    private Library BookIssuedLibrary;
    private int issuedBookId = 0;

    public IssuedBookRecord(Book book, Admin admin, Student student, Library library) {
        setBorrowedStudent(student);
        setBookIssuedAdmin(admin);
        setIssuedBook(book);
        setBookIssuedLibrary(library);

    }

    public Admin getBookIssuedAdmin() {
        return bookIssuedAdmin;
    }

    private void setBookIssuedAdmin(Admin bookIssuedAdmin) {
        this.bookIssuedAdmin = bookIssuedAdmin;
    }

    public Book getIssuedBook() {
        return issuedBook;
    }

    private void setIssuedBook(Book issuedBook) {
        this.issuedBook = issuedBook;
    }

    public Student getBorrowedStudent() {
        return borrowedStudent;
    }

    private void setBorrowedStudent(Student borrowedStudent) {
        this.borrowedStudent = borrowedStudent;
    }

    public Library getBookIssuedLibrary() {
        return BookIssuedLibrary;
    }

    private void setBookIssuedLibrary(Library bookIssuedLibrary) {
        BookIssuedLibrary = bookIssuedLibrary;
    }

    public int getIssuedBookId() {
        return issuedBookId;
    }

    private void setIssuedBookId(int issuedBookId) {
        this.issuedBookId = issuedBookId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof IssuedBookRecord)) return false;
        IssuedBookRecord that = (IssuedBookRecord) o;
        return getIssuedBookId() == that.getIssuedBookId() &&
                Objects.equals(getBookIssuedAdmin(), that.getBookIssuedAdmin()) &&
                Objects.equals(getIssuedBook(), that.getIssuedBook()) &&
                Objects.equals(getBorrowedStudent(), that.getBorrowedStudent()) &&
                Objects.equals(getBookIssuedLibrary(), that.getBookIssuedLibrary());
    }
}

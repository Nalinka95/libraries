package com.company.bo;

import com.company.bo.enums.Subject;

import java.util.Objects;

public class LibraryBookRecord {
    private Book libraryBook;

    public LibraryBookRecord(Book book) {
        setLibraryBook(book);
    }

    public Book getLibraryBook() {
        return libraryBook;
    }

    private void setLibraryBook(Book libraryBook) {
        this.libraryBook = libraryBook;
    }



}

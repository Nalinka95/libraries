package com.company.bo;

import com.company.bo.enums.Degree;
import com.company.bo.enums.Subject;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Student {
    private int rollNumber;
    private String studentName;
    private int batch;
    private Subject subject;
    private Degree studentDegree;
    private List<Book> studentBookArray;

    public Student(int rollNumber, String studentName, int batch, Subject subject, Degree studentDegree) {
        setRollNumber(rollNumber);
        setStudentName(studentName);
        setBatch(batch);
        setSubject(subject);
        setStudentDegree(studentDegree);
        studentBookArray=new ArrayList<Book>();
    }


    public List<Book> getStudentBookArray() {
        return studentBookArray;
    }

    private void setStudentBookArray(List<Book> studentBookArray) {
        this.studentBookArray = studentBookArray;
    }


    public int getRollNumber() {
        return rollNumber;
    }

    private void setRollNumber(int rollNumber) {
        this.rollNumber = rollNumber;
    }

    public String getStudentName() {
        return studentName;
    }

    private void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public int getBatch() {
        return batch;
    }

    private void setBatch(int batch) {
        this.batch = batch;
    }

    public Subject getSubject() {
        return subject;
    }

    private void setSubject(Subject subject) {
        this.subject = subject;
    }

    public Degree getStudentDegree() {
        return studentDegree;
    }

    private void setStudentDegree(Degree studentDegree) {
        this.studentDegree = studentDegree;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Student)) return false;
        Student student = (Student) o;
        return getRollNumber() == student.getRollNumber() &&
                getBatch() == student.getBatch() &&
                Objects.equals(getStudentName(), student.getStudentName()) &&
                getSubject() == student.getSubject() &&
                getStudentDegree() == student.getStudentDegree() &&
                Objects.equals(getStudentBookArray(), student.getStudentBookArray());
    }
}

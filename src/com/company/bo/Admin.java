package com.company.bo;

import java.util.Objects;

public class Admin {
    private String adminName;
    private int identityNumber;

    public Admin(String adminName, int identityNumber) {
        setAdminName(adminName);
        setIdentityNumber(identityNumber);
    }

    public String getAdminName() {
        return adminName;
    }

    private void setAdminName(String adminName) {
        this.adminName = adminName;
    }

    public int getIdentityNumber() {
        return identityNumber;
    }

    private void setIdentityNumber(int identityNumber) {
        this.identityNumber = identityNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Admin)) return false;
        Admin admin = (Admin) o;
        return getIdentityNumber() == admin.getIdentityNumber() &&
                getAdminName().equals(admin.getAdminName());
    }
}

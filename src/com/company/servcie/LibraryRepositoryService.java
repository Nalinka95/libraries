package com.company.servcie;

import com.company.bo.Admin;
import com.company.bo.Book;
import com.company.bo.Library;
import com.company.bo.Student;

public interface LibraryRepositoryService {
    void validateAndIssueBook(Admin admin, Student student, Book book, Library library);
}



package com.company.servcie.impl;

import com.company.bo.*;
import com.company.dao.IssueBookRecordDao;
import com.company.dao.LibraryBookRecordDao;
import com.company.dao.LibraryDao;
import com.company.servcie.LibraryRepositoryService;

import java.util.ArrayList;
import java.util.List;

public class LibraryRepositoryServiceImpl implements LibraryRepositoryService {

    private LibraryDao libraryList;
    private IssueBookRecordDao issueBookRecordList;
    private LibraryBookRecordDao libraryBookRecordDao;

    List<Book> studentBooks= new ArrayList<>();

    public LibraryRepositoryServiceImpl(LibraryDao libraryList, IssueBookRecordDao issueBookRecordList, LibraryBookRecordDao libraryBookRecordDao) {
        setLibraryList(libraryList);
        setIssueBookRecordList(issueBookRecordList);
        setLibraryBookRecordDao(libraryBookRecordDao);

    }

    public void validateAndIssueBook(Admin admin, Student student, Book book, Library library){
        if(libraryList.isValidLibrary(library)){
            issueBookFromLibrary(admin,student,book,library);
        }
    }
    private void issueBookFromLibrary(Admin admin, Student student, Book book, Library library){

        IssuedBookRecord issuingBookRecord= new IssuedBookRecord(book,admin,student,library);
        issueBookRecordList.addIssuedBook(issuingBookRecord);
    }

    public LibraryDao getLibraryList() {
        return libraryList;
    }

    public void setLibraryList(LibraryDao libraryList) {
        this.libraryList = libraryList;
    }

    public IssueBookRecordDao getIssueBookRecordList() {
        return issueBookRecordList;
    }

    public void setIssueBookRecordList(IssueBookRecordDao issueBookRecordList) {
        this.issueBookRecordList = issueBookRecordList;
    }

    public LibraryBookRecordDao getLibraryBookRecordDao() {
        return libraryBookRecordDao;
    }

    public void setLibraryBookRecordDao(LibraryBookRecordDao libraryBookRecordDao) {
        this.libraryBookRecordDao = libraryBookRecordDao;
    }
}
